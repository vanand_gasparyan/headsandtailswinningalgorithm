#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>

enum Prediction
{
	OBVERSE,
	REVERSE
};

Prediction randomSide()
{
	int randomValue = rand() % 2;
	switch (randomValue)
	{
	case 0:
		return OBVERSE;
	case 1:
		return REVERSE;
	}
}

double mean(const std::vector<double>& input)
{
	double meanToReturn = 0.0;
	for (size_t idx = 0; idx < input.size(); ++idx)
	{
		meanToReturn += input[idx];
	}
	return meanToReturn / (double)input.size();
}

int main()
{
	std::srand(time(0));

	size_t numberOfWholePaths = 0;
	std::cout << "Input number of whole paths: ";
	std::cin >> numberOfWholePaths;
	size_t numberOfPaths = 0;
	std::cout << "Input number of paths: ";
	std::cin >> numberOfPaths;

	std::vector<double> moneyBalances;
	std::vector<double> sideBalances;
	std::vector<double> minMoneyBalances;

	for (size_t idx = 0; idx < numberOfWholePaths; ++idx)
	{
		int moneyBalance = 0;
		Prediction lastPrediction = OBVERSE;
		Prediction nextPrediction = OBVERSE;
		int sideBalance = 0;
		int minMoneyBalance = 0;

		for (size_t index = 0; index < numberOfPaths; ++index)
		{
			if (sideBalance > 0)
				nextPrediction = REVERSE;
			else if (sideBalance < 0)
				nextPrediction = OBVERSE;
			else
				nextPrediction = (lastPrediction == OBVERSE) ? REVERSE : OBVERSE;

			switch (randomSide())
			{
			case OBVERSE:
			{
				++sideBalance;

				if (nextPrediction == OBVERSE)
					++moneyBalance;
				else
					--moneyBalance;

				break;
			}
			case REVERSE:
			{
				--sideBalance;

				if (nextPrediction == REVERSE)
					++moneyBalance;
				else
					--moneyBalance;

				break;
			}
			}

			lastPrediction = nextPrediction;
			minMoneyBalance = (moneyBalance < minMoneyBalance) ? moneyBalance : minMoneyBalance;
		}

		std::cout << moneyBalance << '\t' << sideBalance << '\t' << minMoneyBalance <<'\n';
		moneyBalances.push_back((double)moneyBalance);
		sideBalances.push_back((double)sideBalance);
		minMoneyBalances.push_back((double)minMoneyBalance);
	}

	std::cout << '\n';
	std::cout << "Mean moneyBalance: " << mean(moneyBalances) << '\n';
	std::cout << "Mean sideBalance: " << mean(sideBalances) << '\n';
	std::cout << "Mean minMoneyBalances: " << mean(minMoneyBalances) << '\n';
}